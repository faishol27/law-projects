package main

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"github.com/bshuster-repo/logrus-logstash-hook"
    "github.com/sirupsen/logrus"
    "net"
	"time"
)

type Person struct {
	Name     string    `form:"name"`
}

var logger = logrus.New()

func main() {
	initLogger()
	route := gin.Default()
	route.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		logMessage := fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
		logger.Info(logMessage)
		return logMessage
	}))
	route.GET("/sapa", sapa)
	route.Run() // listen and serve on 0.0.0.0:8080
}

func sapa(c *gin.Context) {
	var person Person
	var response = "Please enter your name!"
	if err := c.ShouldBind(&person), err != nil {
		logger.Fatal(err)
		c.String(400, "Bad request")
		return
	}

	if person.Name != "" {
		response = fmt.Sprintf("Halo %s!", person.Name)
	}
	
	c.String(200, response)
}

func initLogger() {
    conn, err := net.Dial("tcp", "34.132.46.126:5000") 
    if err != nil {
        logger.Fatal(err)
    }
    hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields{
		"type": "logs",
		"task": "LAW - Tugas Mandiri 3",
	}))
    logger.Hooks.Add(hook)
}
