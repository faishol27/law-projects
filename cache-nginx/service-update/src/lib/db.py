import sqlite3

class Database():
    db = None

    def __get_db(self):
        if self.db == None:
            self.db = sqlite3.connect('db.sqlite3', check_same_thread=False)
        return self.db

    def query_db(self, query, args=(), one=False):
        cur = self.__get_db().execute(query, args)
        rv = cur.fetchall()
        self.__get_db().commit()
        cur.close()
        return (rv[0] if rv else None) if one else rv
    
    def destroy(self):
        db = self.__get_db()
        if db is not None:
            db.close()