import sqlite3
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

with sqlite3.connect(BASE_DIR / 'db.sqlite3') as con:
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS mahasiswa(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        npm varchar(100) NOT NULL,
        nama varchar(255) NOT NULL
    )""")