import sqlite3
from pathlib import Path
from random import randint
from secrets import token_hex, choice

with open('/usr/share/dict/words') as f:
    words = [word.strip() for word in f]

BASE_DIR = Path(__file__).resolve().parent.parent

with sqlite3.connect(BASE_DIR / 'db.sqlite3') as con:
    cur = con.cursor()
    cur.execute("""CREATE TABLE users(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username varchar(100) NOT NULL UNIQUE,
        password varchar(255) NOT NULL,
        full_name varchar(255) NOT NULL,
        npm varchar(10) NOT NULL
    )""")
    cur.execute("""CREATE TABLE clients(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        secret varchar(20) NOT NULL
    )""")
    print("[OAUTH CLIENTS]")
    for i in range(2):
        tkn = token_hex(10)
        cur.execute(f"""INSERT INTO clients(secret) VALUES(?)""", [tkn])
        print(i+1, tkn, sep=":")
    print("[USERS]")
    for full_name in ['Atari Maidarou', 'Kon Deaw']:
        username = full_name.split(" ")[0].lower()
        password = '-'.join(choice(words) for i in range(4))
        npm = randint(10 ** 9, 10 ** 10 - 1)
        cur.execute(f"INSERT INTO users(username,password,full_name,npm) VALUES(?, ?, ?, ?)", [username, password, full_name, npm])
        print(username,password,full_name,npm, sep=":")