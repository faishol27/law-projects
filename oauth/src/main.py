from flask import Flask, request, g
from secrets import token_hex
from utils.db import query_db
from utils.redis import get_key, set_key

app = Flask(__name__)

EXPIRES = 60 * 5

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
    r = getattr(g, "_redis", None)
    if r is not None:
        r.close()

def check_client(cid, csecret):
    client = query_db(
        "SELECT * FROM clients WHERE id = ? AND secret = ?",
        [cid, csecret],
        one = True)
    return client

def authenticate(username, password):
    return query_db(
        "SELECT id FROM users WHERE username = ? AND password = ?",
        [username, password],
        one = True
    )

def get_userinfo(userid):
    user = query_db(
        "SELECT id, full_name, npm FROM users WHERE id = ?",
        [userid],
        one = True
    )
    if user:
        return {
            "user_id": user[0],
            "full_name": user[1],
            "npm": user[2]
        }
    return None

def generate_token(cid, userid):
    token = token_hex(20)
    set_key(token, f"{cid}:{userid}")
    return token

def claim_token(token):
    v = get_key(token)
    if v:
        cid, userid = v.decode().split(":")
        user = get_userinfo(userid)
        if user:
            user['client_id'] = cid
        return user
    return None

@app.route("/oauth/token", methods=['POST'])
def get_token():
    if check_client(request.form['client_id'], request.form['client_secret']):
        user = authenticate(request.form['username'], request.form['password'])
        if user:
            return {
                "access_token": generate_token(request.form['client_id'], user[0]),
                "expires_in": EXPIRES,
                "token_type": "Bearer",
                "scope": None,
                "refresh_token": generate_token(request.form['client_id'], user[0])
            }
        
    return {'error': 'invalid_token', 'error_description': 'ada kesalahan masbro'}, 401

@app.route("/oauth/resource", methods=["POST"])
def get_myresource():
    auth_header = request.headers['Authorization']
    if auth_header:
        auth_header = str(auth_header).split(" ")
        if auth_header[0] == 'Bearer':
            resp = claim_token(auth_header[1])
            if resp:
                resp['access_token'] = auth_header[1]
                resp['refresh_token'] = generate_token(resp['client_id'], resp['user_id'])
                resp['expires'] = None
                return resp

    return {'error': 'invalid_token', 'error_description': 'Token Salah masbro'}, 401

if __name__ == "__main__":
    app.run(host="0.0.0.0")